import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyDnvwWIBGjVzooAlcco3GFaDOJBHEmSME8",
  authDomain: "shorterlink1.firebaseapp.com",
  projectId: "shorterlink1",
  databaseURL: "https://shorterlink1-default-rtdb.firebaseio.com",
  storageBucket: "shorterlink1.appspot.com",
  messagingSenderId: "821197756739",
  appId: "1:821197756739:web:42eb4bf422129d895c77f6",
  measurementId: "G-E6WRPV9NKY"
};

initializeApp(firebaseConfig);

ReactDOM.render(
  <BrowserRouter>
      <App />
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
